//go:build test

package mr

import (
	"fmt"
	"sort"
	"testing"
)

func quickMinHeaped() KeyValueList {
	kvl := make(KeyValueList, 10)
	kvl[0] = KeyValue{Key: "01", Value: "blah"}
	kvl[1] = KeyValue{Key: "05", Value: "blah"}
	kvl[2] = KeyValue{Key: "03", Value: "blah"}
	kvl[3] = KeyValue{Key: "08", Value: "blah"}
	kvl[4] = KeyValue{Key: "07", Value: "blah"}
	kvl[5] = KeyValue{Key: "04", Value: "blah"}
	kvl[6] = KeyValue{Key: "05", Value: "blah"}
	kvl[7] = KeyValue{Key: "12", Value: "blah"}
	kvl[8] = KeyValue{Key: "99", Value: "blah"}
	kvl[9] = KeyValue{Key: "55", Value: "blah"}
	return kvl
}

func messedUpArray() KeyValueList {
	kvl := make(KeyValueList, 10)
	kvl[0] = KeyValue{Key: "04", Value: "blah"}
	kvl[1] = KeyValue{Key: "08", Value: "blah"}
	kvl[2] = KeyValue{Key: "05", Value: "blah"}
	kvl[3] = KeyValue{Key: "07", Value: "blah"}
	kvl[4] = KeyValue{Key: "99", Value: "blah"}
	kvl[5] = KeyValue{Key: "05", Value: "blah"}
	kvl[6] = KeyValue{Key: "55", Value: "blah"}
	kvl[7] = KeyValue{Key: "03", Value: "blah"}
	kvl[8] = KeyValue{Key: "12", Value: "blah"}
	kvl[9] = KeyValue{Key: "01", Value: "blah"}
	return kvl
}

func sorted() KeyValueList {
	kvl := make(KeyValueList, 10)
	kvl[0] = KeyValue{Key: "01", Value: "blah"}
	kvl[1] = KeyValue{Key: "02", Value: "blah"}
	kvl[2] = KeyValue{Key: "03", Value: "blah"}
	kvl[3] = KeyValue{Key: "04", Value: "blah"}
	kvl[4] = KeyValue{Key: "04", Value: "blah"}
	kvl[5] = KeyValue{Key: "05", Value: "blah"}
	kvl[6] = KeyValue{Key: "06", Value: "blah"}
	kvl[7] = KeyValue{Key: "07", Value: "blah"}
	kvl[8] = KeyValue{Key: "08", Value: "blah"}
	kvl[9] = KeyValue{Key: "09", Value: "blah"}
	return kvl
}

func dummySource(length int) []int {
	return make([]int, length)
}

func TestChildrenMath(t *testing.T) {
	h := keyValueHeap{}
	if h.leftChild(0) != 1 ||
		h.rightChild(0) != 2 ||
		h.leftChild(1) != 3 ||
		h.rightChild(1) != 4 ||
		h.leftChild(2) != 5 ||
		h.rightChild(2) != 6 {
		t.Fail()
	}
}

func TestIRChecker(t *testing.T) {
	minHeap := keyValueHeap{data: quickMinHeaped(), source: dummySource(10)}
	sorted := keyValueHeap{data: sorted(), source: dummySource(10)}
	if !sorted.IRCheck() ||
		!minHeap.IRCheck() {
		t.Fail()
	}
}

func TestPushUp(t *testing.T) {
	h := keyValueHeap{data: quickMinHeaped(), source: dummySource(10)}
	h.Push(KeyValue{"01", "blah"}, 0)
	if !h.IRCheck() {
		t.Fail()
	}
}

func TestPushDown(t *testing.T) {
	h := keyValueHeap{data: sorted(), source: dummySource(10)}
	h.data[0].Key = "99"
	if h.IRCheckElem(0) {
		t.Fail()
	}
	h.pushDown(0)
	if !h.IRCheck() {
		t.Fail()
	}
}

func TestAbstractMin(t *testing.T) {
	modfied := keyValueHeap{data: sorted(), source: dummySource(10)}
	i := 0
	cmp := keyValueHeap{data: sorted(), source: dummySource(10)}
	l := cmp.Len() - 1
	for i < l {
		abs, _ := modfied.PopMin()
		if abs.data.ToString() != cmp.data[i].ToString() || l != modfied.Len() {
			fmt.Println("l:", l, "len:", modfied.Len())
			fmt.Println("out:", abs.data.ToString(), "\nexpected:", cmp.data[i].ToString())
			t.FailNow()
		}
		i++
		l--
	}
}

func TestFromArray(t *testing.T) {
	arr := messedUpArray()
	h := FromArray(arr)
	if !h.IRCheck() {
		t.Fail()
	}
}

func TestMerging(t *testing.T) {
	arr1 := sorted()
	arr2 := sorted()
	arr3 := sorted()
	arr4 := sorted()
	merger := MergerFromData([]KeyValueList{arr1, arr2, arr3, arr4})
	i := 0
	for i < 10 {
		e1, _ := merger.popMin()
		e2, _ := merger.popMin()
		e3, _ := merger.popMin()
		e4, _ := merger.popMin()
		if e1 != arr1[i] ||
			e2 != arr1[i] ||
			e3 != arr1[i] ||
			e4 != arr1[i] {
			t.Fail()
		}
		i++
	}
	arr10 := KeyValueList(shuffledArray[:10])
	arr20 := KeyValueList(shuffledArray[10:20])
	arr30 := KeyValueList(shuffledArray[20:30])
	arr40 := KeyValueList(shuffledArray[30:40])
	arr50 := KeyValueList(shuffledArray[40:50])
	arr60 := KeyValueList(shuffledArray[50:60])
	arr70 := KeyValueList(shuffledArray[60:70])
	arr80 := KeyValueList(shuffledArray[70:80])
	arr90 := KeyValueList(shuffledArray[80:90])
	arr100 := KeyValueList(shuffledArray[90:100])
	sort.Sort(arr10)
	sort.Sort(arr20)
	sort.Sort(arr30)
	sort.Sort(arr40)
	sort.Sort(arr50)
	sort.Sort(arr60)
	sort.Sort(arr70)
	sort.Sort(arr80)
	sort.Sort(arr90)
	sort.Sort(arr100)
	merger2 := MergerFromData([]KeyValueList{
		arr10,
		arr20,
		arr30,
		arr40,
		arr50,
		arr60,
		arr70,
		arr80,
		arr90,
		arr100,
	})
	i = 0
	for i < 100 {
		e, err := merger2.popMin()
		if err != nil {
			t.Fail()
		}
		if fmt.Sprintf("%03d", i+1) != e.Key {
			fmt.Println("extracted:", fmt.Sprintf("%03d", i+1))
			fmt.Println("found:", e)
			t.Fail()
		}
		i++
	}
	_, err := merger2.popMin()
	if err != Empty(0) {
		fmt.Println("expected empty merger")
		t.Fail()
	}
}
