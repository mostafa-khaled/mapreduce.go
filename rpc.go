package mr

//
// RPC definitions.
//

import (
	"fmt"
	"log"
	"os"
	"strconv"
)

type JobID int
type JobType int

const (
	MapJob JobType = iota
	ReduceJob
	none
	idle
)

type Job struct {
	Type       JobType
	Input      []string
	OutputBase string
	NOutput    int
}

type WorkerJob struct {
	ID  JobID
	Job Job
}

func noneJob() Job {
	return Job{none, []string{"/dev/null"}, "/dev/null", 0}
}

func holdIdle() Job {
	return Job{idle, []string{"/dev/null"}, "/dev/null", 0}
}

func TypeToString(t JobType) string {
	switch t {
	case none:
		return "None"
	case idle:
		return "Idle"
	case MapJob:
		return "Map"
	case ReduceJob:
		return "Reduce"
	}
	log.Fatalln("unknown job type")
	return "nil"
}

func (job *Job) ToString() string {
	return fmt.Sprintf("input file: %s\noutput base: %s\noutput number: %d\ntype: %s",
		job.Input,
		job.OutputBase,
		job.NOutput,
		TypeToString(job.Type),
	)
}

func (job *Job) OutputFiles(id WorkerID, extraDist int) []string {
	if job.Type != MapJob {
		fmt.Printf("invalid operation.\nTrying to get output files of job: %s", job.ToString())
	}
	output := make([]string, 10)
	i := 0
	for i < job.NOutput {
		output[i] = fmt.Sprintf("%s%d-%d-%d", job.OutputBase, id, extraDist, i)
		i++
	}
	return output
}

type WorkerID int

type JobReqArgs WorkerID

type JobReqResp WorkerJob

type JobFinishedNotificationArgs struct {
	Id JobID
	Jn int
}
type JobFinishedNotificationResp int

// Add your RPC definitions here.

// Cook up a unique-ish UNIX-domain socket name
// in /var/tmp, for the coordinator.
// Can't use the current directory since
// Athena AFS doesn't support UNIX-domain sockets.
func coordinatorSock() string {
	s := "/var/tmp/824-mr-"
	s += strconv.Itoa(os.Getuid())
	return s
}
