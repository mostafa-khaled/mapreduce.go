package mr

// TODO: fix the check ir to check for indecies too

import (
	"log"
)

type heapEntry struct {
	data KeyValue
	// source index
	source int
}

type keyValueHeap struct {
	source []int
	data   []KeyValue
}

func (_ *keyValueHeap) hasParent(ind int) bool {
	return ind != 0
}

func (_ *keyValueHeap) parent(ind int) int {
	if ind%2 == 1 {
		return ind / 2
	} else {
		return ind/2 - 1
	}
}

func (h *keyValueHeap) hasChild(ind int) bool {
	return h.leftChild(ind) < h.Len()
}

func (h *keyValueHeap) leftChild(ind int) int {
	return ind*2 + 1
}

func (h *keyValueHeap) rightChild(ind int) int {
	return (ind + 1) * 2
}

func (h *keyValueHeap) less(a, b int) bool {
	return h.data[a].Key < h.data[b].Key
}

func (h *keyValueHeap) min(a, b int) int {
	if h.less(a, b) {
		return a
	} else {
		return b
	}
}

func (h *keyValueHeap) minChild(ind int) int {
	if !h.hasChild(ind) {
		log.Fatalln("index out of range")
		return 0
	} else {
		minC := h.leftChild(ind)
		if h.rightChild(ind) < h.Len() {
			minC = h.min(minC, h.rightChild(ind))
		}
		return minC
	}
}

func (h *keyValueHeap) swap(a, b int) {
	h.data[a], h.data[b] = h.data[b], h.data[a]
	h.source[a], h.source[b] = h.source[b], h.source[a]
}

func (h *keyValueHeap) pushDown(ind int) {
	for h.hasChild(ind) {
		minC := h.minChild(ind)
		if h.less(minC, ind) {
			h.swap(minC, ind)
			ind = minC
		} else {
			break
		}
	}
}

func (h *keyValueHeap) pushUp(ind int) {
	for h.hasParent(ind) {
		par := h.parent(ind)
		if h.less(ind, par) {
			h.swap(ind, par)
			ind = par
		} else {
			break
		}
	}
}

func (h *keyValueHeap) Len() int {
	return len(h.data)
}

func (h *keyValueHeap) PopMin() (heapEntry, error) {
	if h.Len() == 0 {
		return heapEntry{}, Empty(0)
	}

	// NOTE: This should be copied
	entry := heapEntry{h.data[0], h.source[0]}

	// delete first element
	h.swap(0, h.Len()-1)
	// old length gets changed!
	lockedLen := h.Len()
	h.data = h.data[:lockedLen-1]
	h.source = h.source[:lockedLen-1]

	h.pushDown(0)
	return entry, nil
}

func (h *keyValueHeap) Push(elem KeyValue, source int) {
	h.data = append(h.data, elem)
	h.source = append(h.source, source)
	h.pushUp(h.Len() - 1)
}

func (h *keyValueHeap) hasLeftChild(ind int) bool {
	return h.hasChild(ind)
}

func (h *keyValueHeap) hasRightChild(ind int) bool {
	return h.rightChild(ind) < h.Len()
}

func (h *keyValueHeap) IRCheckElem(ind int) bool {
	if h.hasRightChild(ind) && h.less(h.rightChild(ind), ind) {
		return false
	} else if h.hasLeftChild(ind) && h.less(h.leftChild(ind), ind) {
		return false
	} else {
		return true
	}
}

func (h *keyValueHeap) IRCheck() bool {
	ind := 0
	for ind < h.Len() {
		if !h.hasLeftChild(ind) {
			return true
		} else {
			if !h.IRCheckElem(ind) {
				return false
			}
		}
		ind++
	}
	return true
}

// this assumes that the source is sorted 0, 1, 2, ...
func FromArray(arr []KeyValue) keyValueHeap {
	h := keyValueHeap{data: arr, source: make([]int, len(arr))}
	i := len(arr)
	for i > 0 {
		// order matters alot
		// note that pushdown does not change anything above the given index
		h.source[i-1] = i - 1
		h.pushDown(i - 1)
		i--
	}
	return h
}

type fileData struct {
	kvl      KeyValueList
	startInd int
}

type IntermediateMerger struct {
	files      []fileData
	prioQueue  keyValueHeap
	emptyFiles int
}

func (merger *IntermediateMerger) Size() int {
	sum := 0
	for _, file := range merger.files {
		sum += len(file.kvl)
	}
	return sum
}

func (l *fileData) Len() int {
	return l.kvl.Len() - l.startInd
}

func (l *fileData) PopFront() {
	l.startInd += 1
}

func (l *fileData) Front() KeyValue {
	return l.kvl[l.startInd]
}

func (l *fileData) Empty() bool {
	return l.startInd >= len(l.kvl)
}

type Empty int

func (_ Empty) Error() string {
	return "empty"
}

func (merger *IntermediateMerger) popMin() (KeyValue, error) {
	if merger.emptyFiles == len(merger.files) {
		return KeyValue{"", ""}, Empty(0)
	} else {
		kvEntry, err := merger.prioQueue.PopMin()
		if err != nil {
			return kvEntry.data, err
		}
		merger.files[kvEntry.source].PopFront()
		if !merger.files[kvEntry.source].Empty() {
			merger.prioQueue.Push(merger.files[kvEntry.source].Front(), kvEntry.source)
		} else {
			merger.emptyFiles += 1
		}
		kv := kvEntry.data
		return kv, nil
	}
}

func MergerFromData(kvll []KeyValueList) IntermediateMerger {
	filesList := make([]fileData, len(kvll))
	prioQArray := make(KeyValueList, len(kvll))
	i := 0
	for i < len(kvll) {
		filesList[i] = fileData{kvll[i], 0}
		prioQArray[i] = kvll[i][0]
		i++
	}
	prioQ := FromArray(prioQArray)
	merger := IntermediateMerger{filesList, prioQ, 0}
	return merger
}

func MergerFromFiles(files []string) IntermediateMerger {
	d := make([]KeyValueList, len(files))
	for i, file := range files {
		var ok error
		d[i], ok = FileToKVL(file)
		if ok != nil {
			log.Fatalln("failed to read file: " + file)
		}
	}
	return MergerFromData(d)
}

func (merger *IntermediateMerger) InitPrioQueue() {
	prioQueue := KeyValueList{}
	i := 0
	for i < len(merger.files) {
		if merger.files[i].Len() != 0 {
			prioQueue = append(prioQueue, merger.files[i].Front())
		}
		i++
	}
	merger.prioQueue = FromArray(prioQueue)
}

// TODO: return it compressed
func merge(files []string) []KeyValue {
	merger := MergerFromFiles(files)
	mergerSize := merger.Size()
	merged := make([]KeyValue, mergerSize)
	i := 0
	for i < mergerSize {
		merged[i], _ = merger.popMin()
		i++
	}
	return merged
}
