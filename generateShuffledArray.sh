#!/bin/bash

outputfile='shuffledArray.go'

cat << EOF > $outputfile
//go:build test

package mr

var shuffledArray = []KeyValue {
EOF
for i in {001..100}; do echo "{\""$i"\", \"\"},"; done | shuf >> $outputfile
echo '}' >> $outputfile

go fmt $outputfile
