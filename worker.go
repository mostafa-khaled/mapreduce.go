package mr

import (
	"fmt"
	"hash/fnv"
	"log"
	"net/rpc"
	"os"
	"sort"
	"time"
)

// use ihash(key) % NReduce to choose the reduce
// task number for each KeyValue emitted by Map.
func ihash(key string) int {
	h := fnv.New32a()
	h.Write([]byte(key))
	return int(h.Sum32() & 0x7fffffff)
}

func exec(mapf func(string, string) []KeyValue, reducef func(string, []string) string, workerJob WorkerJob, jobN int) {
	job := workerJob.Job
	workerID := workerJob.ID
	switch job.Type {
	case MapJob:
		inputFile := job.Input[0]
		// TODO: write in /tmp dir
		cont := quickRead(inputFile)
		kvps := mapf(inputFile, cont)
		intermediateBuckets := make([][]KeyValue, job.NOutput)
		for _, kvp := range kvps {
			insertBucketIndex := ihash(kvp.Key) % job.NOutput
			intermediateBuckets[insertBucketIndex] = append(intermediateBuckets[insertBucketIndex], kvp)
		}

		for _, bucket := range intermediateBuckets {
			sort.Sort(KeyValueList(bucket))
		}

		for _, fileName := range job.OutputFiles(WorkerID(workerID), jobN) {
			var writeString string
			for _, bucket := range intermediateBuckets {
				for _, kvp := range bucket {
					writeString += kvp.ToString() + "\n"
				}
			}
			os.Remove(fileName)
			file, err := os.Create(fileName)
			if err != nil {
				fmt.Println("failed to open file:", file)
				fmt.Println("error:", err)
				os.Exit(1)
			}
			file.WriteString(writeString)
			file.Close()
		}
	case ReduceJob:
		// TODO: handle empty reduce requests
		oname := fmt.Sprintf("%s%d", job.OutputBase, job.NOutput)
		ofile, _ := os.Create(oname)
		ifiles := job.Input
		merged := merge(ifiles)
		firstKVP := merged[0]
		curKey := firstKVP.Key
		curKeyVals := []string{firstKVP.Value}
		for _, kvp := range merged[1:] {
			if kvp.Key == curKey {
				curKeyVals = append(curKeyVals, kvp.Value)
			} else {
				ofile.WriteString(fmt.Sprintf("%s %s\n", curKey, reducef(curKey, curKeyVals)))
				curKey = firstKVP.Key
				curKeyVals = []string{firstKVP.Value}
			}
		}
		ofile.WriteString(fmt.Sprintf("%s %s\n", curKey, reducef(curKey, curKeyVals)))
		ofile.Close()
	}
}

// main/mrworker.go calls this function.
func Worker(mapf func(string, string) []KeyValue,
	reducef func(string, []string) string) {
	jobNumber := 0
	for {
		resp := ReqJob()
		if resp.Job.Type == none {
			return
		} else if resp.Job.Type == idle {
			time.Sleep(time.Millisecond * 100)
		} else if resp.Job.Type == MapJob || resp.Job.Type == ReduceJob {
			exec(mapf, reducef, resp, jobNumber)
			jobNumber += 1
			NotifyFinished(resp.ID, jobNumber)
		} else {
			fmt.Println("unknown job type")
			os.Exit(1)
		}
	}
}

func NotifyFinished(ID JobID, jn int) int {
	resp := 0
	call("HttpServer.NotifyFinished", JobFinishedNotificationArgs{ID, jn}, &resp)
	return resp
}

func ReqJob() WorkerJob {
	args := WorkerID(os.Getpid())
	var reply WorkerJob
	ok := call("HttpServer.ReqJob", &args, &reply)
	if ok {
		fmt.Printf("task assigend:\n%s\n", reply.Job.ToString())
	} else {
		fmt.Println("failed to get job")
		os.Exit(1)
	}
	return reply
}

// send an RPC request to the coordinator, wait for the response.
// usually returns true.
// returns false if something goes wrong.
func call(rpcname string, args interface{}, reply interface{}) bool {
	// c, err := rpc.DialHTTP("tcp", "127.0.0.1"+":1234")
	sockname := coordinatorSock()
	c, err := rpc.DialHTTP("unix", sockname)
	if err != nil {
		log.Fatal("dialing:", err)
	}
	defer c.Close()

	err = c.Call(rpcname, args, reply)
	if err == nil {
		return true
	}

	log.Println(err)
	return false
}
