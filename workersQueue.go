package mr

import (
	"log"
	"time"
)

type messyQueue struct {
	q    dlist
	eMap map[WorkerID]*dnode
}

type dnode struct {
	data dnodeElem
	next *dnode
	back *dnode
}

type dlist struct {
	front  *dnode
	back   *dnode
	length int
}

func (l *dlist) insertFirst(data dnodeElem) *dnode {
	l.front = &dnode{data: data, next: nil, back: nil}
	l.back = l.front
	l.length = 1
	return l.front
}

func (l *dlist) Prepend(data dnodeElem) *dnode {
	car := &dnode{data: data}
	if l.front == nil {
		return l.insertFirst(data)
	} else {
		car.next = l.front
		car.back = nil
		l.front = car
		l.length += 1
		return car
	}
}

func (l *dlist) Append(data dnodeElem) *dnode {
	car := &dnode{data: data}
	if l.front == nil {
		return l.insertFirst(data)
	} else {
		car.back = l.back
		car.next = nil
		l.back = car
		l.length += 1
		return car
	}
}

func (l *dlist) Delete(node *dnode) {
	if node == nil {
		log.Fatalln("deleting node with address", node)
	}
	l.length -= 1
	if node == l.back {
		l.back = node.back
	}
	if node == l.front {
		l.front = node.next
	}
	if node.next != nil {
		node.next.back = node.back
	}
	if node.back != nil {
		node.back.next = node.next
	}
}

func (l *dlist) Len() int {
	return l.length
}

type dnodeElem struct {
	id     WorkerID
	inTime time.Time
}

func (q *messyQueue) Enqueue(id WorkerID) {
	inTime := time.Now()
	q.eMap[id] = q.q.Append(dnodeElem{id, inTime})
}

func (q *messyQueue) Remove(id WorkerID) {
	q.q.Delete(q.eMap[id])
	delete(q.eMap, id)
}

func (q *messyQueue) RemoveOutdated() []WorkerID {
	outdated := []WorkerID{}
	for q.q.front != nil {
		elem := q.q.front.data
		inTime := elem.inTime
		id := elem.id
		timeTaken := time.Since(inTime)
		if timeTaken >= time.Second*10 {
			outdated = append(outdated, id)
			q.Remove(id)
		} else {
			break
		}
	}
	return outdated
}

func (q *messyQueue) Contains(id WorkerID) bool {
	_, exist := q.eMap[id]
	return exist
}

func (q *messyQueue) Len() int {
	return q.q.Len()
}

func (q *messyQueue) FrontTime() time.Time {
	if q.q.front == nil {
		return time.Now()
	}
	return q.q.front.data.inTime
}
