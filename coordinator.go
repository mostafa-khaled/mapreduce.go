package mr

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"net/rpc"
	"os"
	"strconv"
	"strings"
	"time"
)

const tmpPrefixFmt = "/tmp/go-mr-%v"
const perFilePrefix = "mr-out-"

type assigedTasks struct {
	queue messyQueue
	jobs  map[WorkerID]Job
}

type Coordinator struct {
	nReduce     int
	nMap        int
	currentTask JobType
	// The WorkerID is used to index the status array.
	assigedTasks  assigedTasks
	todoTasks     []Job
	finishedTasks int

	ReduceBuckets [][]string

	apiCommChan  chan string
	httpCommChan chan string
	httpJobsChan chan WorkerJob

	tmpdirPrefix string
	inputfiles   []string
}

func (c *Coordinator) reduceJobs() []Job {
	rjobs := make([]Job, c.nReduce)
	i := 0
	for i < c.nReduce {
		rjobs[i] = Job{OutputBase: perFilePrefix, NOutput: i, Type: ReduceJob, Input: c.ReduceBuckets[i]}
		i++
	}
	return rjobs
}

func mapJobs(files []string, nReduce int, outPrefix string) []Job {
	var jobs []Job
	for _, file := range files {
		job := Job{Type: MapJob, Input: []string{file}, OutputBase: fmt.Sprintf("%s/%s", outPrefix, perFilePrefix), NOutput: nReduce}
		jobs = append(jobs, job)
	}
	return jobs
}

func (c *Coordinator) reqJob(id WorkerID) WorkerJob {
	if len(c.todoTasks) > 0 {
		job := c.todoTasks[len(c.todoTasks)-1]
		c.todoTasks = c.todoTasks[:len(c.todoTasks)-1]
		c.assigedTasks.queue.Enqueue(id)
		return WorkerJob{JobID(id), job}
	} else if c.currentTask == MapJob {
		return WorkerJob{JobID(id), holdIdle()}
	} else {
		return WorkerJob{JobID(id), noneJob()}
	}
}

func (c *Coordinator) updateReduceBuckets(id WorkerID, jn int) {
	i := 0
	for i < c.nReduce {
		c.ReduceBuckets[i] = append(c.ReduceBuckets[i],
			fmt.Sprintf("%s/%s%d-%d-%d",
				c.tmpdirPrefix,
				perFilePrefix,
				id,
				jn-1,
				i,
			))
		i++
	}
}

// TODO: make it handle dropped workers properly
// TODO: assign duplicate jobs when there is excess workers
func (c *Coordinator) notifyFinished(id WorkerID, jn int) {
	if c.assigedTasks.queue.Contains(id) {
		fmt.Println("task", id, "finished")
		if c.currentTask == MapJob {
			c.updateReduceBuckets(id, jn)
		}
		c.assigedTasks.queue.Remove(id)
		c.finishedTasks += 1
		// this assumes that there is no duplicate jobs
		if c.finishedTasks == c.nMap {
			c.todoTasks = c.reduceJobs()
		} else if c.finishedTasks == c.nMap+c.nReduce {
			c.currentTask = none
		}

	}
}

func (c *Coordinator) updateOutdatedTasks() {
	outdatedTasks := c.assigedTasks.queue.RemoveOutdated()
	for _, task := range outdatedTasks {
		c.todoTasks = append(c.todoTasks, c.assigedTasks.jobs[task])
		delete(c.assigedTasks.jobs, task)
	}
}

// keeps track of time taken by workers
// the channel is used to send new jobs
// msg format <func> <worker id>
func (c *Coordinator) runBookKeeper() {
	for {
		var iterSleep time.Duration
		if c.assigedTasks.queue.Len() == 0 {
			iterSleep = 100 * time.Second
		} else {
			iterSleep = 10*time.Second - time.Since(c.assigedTasks.queue.FrontTime())
		}
		select {
		case <-time.After(iterSleep):
			c.updateOutdatedTasks()
		case apiReq := <-c.apiCommChan:
			switch apiReq {
			case "done":
				if c.finishedTasks == c.nReduce+c.nMap {
					c.apiCommChan <- "1"
					ShellCmd(fmt.Sprintf("rm %s -rf", c.tmpdirPrefix))
				} else {
					c.apiCommChan <- "0"
				}
			}
		case req := <-c.httpCommChan:
			reqSlice := strings.Fields(req)
			workerID, ok := strconv.Atoi(reqSlice[1])
			if ok != nil {
				log.Fatal("can not convert " + reqSlice[1] + " to integer")
			}
			switch reqSlice[0] {
			case "reqJob":
				assigedJob := c.reqJob(WorkerID(workerID))
				c.httpJobsChan <- assigedJob
			case "notifyFinished":
				JobNumber, ok := strconv.Atoi(reqSlice[2])
				if ok != nil {
					log.Fatal("can not convert " + reqSlice[1] + " to integer")
				}
				c.notifyFinished(WorkerID(workerID), JobNumber)
			}
		}
	}
}

type HttpServer struct {
	ReqsChan chan string
	RespChan chan WorkerJob
}

func (server *HttpServer) ReqJob(args JobReqArgs, reply *JobReqResp) error {
	server.ReqsChan <- "reqJob " + fmt.Sprintf("%d", args)
	job := JobReqResp(<-server.RespChan)
	reply.Job.OutputBase = job.Job.OutputBase
	reply.Job.Type = job.Job.Type
	reply.Job.Input = job.Job.Input
	reply.Job.NOutput = job.Job.NOutput
	reply.ID = job.ID
	return nil
}

func (server *HttpServer) NotifyFinished(args JobFinishedNotificationArgs, resp *JobFinishedNotificationResp) error {
	server.ReqsChan <- "notifyFinished " + fmt.Sprintf("%d %d", args.Id, args.Jn)
	*resp = 0
	return nil
}

// the RPC argument and reply types are defined in rpc.go.
// start a thread that listens for RPCs from worker.go
func (server *HttpServer) server() {
	rpc.Register(server)
	rpc.HandleHTTP()
	//l, e := net.Listen("tcp", ":1234")
	sockname := coordinatorSock()
	os.Remove(sockname)
	l, e := net.Listen("unix", sockname)
	if e != nil {
		log.Fatal("listen error:", e)
	}
	go http.Serve(l, nil)
}

// main/mrcoordinator.go calls Done() periodically to find out
// if the entire job has finished.
func (c *Coordinator) Done() bool {
	c.apiCommChan <- "done 0"
	return "1" == <-c.apiCommChan
}

// create a Coordinator.
// main/mrcoordinator.go calls this function.
// nReduce is the number of reduce tasks to use.
func MakeCoordinator(files []string, nReduce int) *Coordinator {
	httpCommChan := make(chan string)
	httpJobsChan := make(chan WorkerJob)
	apiCommChan := make(chan string)
	tmpdir := fmt.Sprintf(tmpPrefixFmt, os.Getpid())
	c := Coordinator{
		nReduce:     nReduce,
		nMap:        len(files),
		currentTask: MapJob,
		assigedTasks: assigedTasks{
			messyQueue{eMap: make(map[WorkerID]*dnode), q: dlist{}},
			make(map[WorkerID]Job),
		},
		todoTasks:     mapJobs(files, nReduce, tmpdir),
		finishedTasks: 0,
		ReduceBuckets: make([][]string, nReduce),
		apiCommChan:   apiCommChan,
		httpJobsChan:  httpJobsChan,
		httpCommChan:  httpCommChan,
		tmpdirPrefix:  tmpdir,
	}
	ShellCmd(fmt.Sprintf("mkdir %s", c.tmpdirPrefix))

	/* book keeping thread */
	go c.runBookKeeper()

	/* http server thread */
	httpserver := HttpServer{httpCommChan, httpJobsChan}

	httpserver.server()
	return &c
}
