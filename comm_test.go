package mr

import (
	"testing"
)

func TestToString(t *testing.T) {
	kv := KeyValue{"001", "test"}
	if kv.ToString() != "001 test" {
		t.Fail()
	}
}
