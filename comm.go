package mr

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	shellExec "os/exec"
	"strings"
)

type KeyValue struct {
	Key   string
	Value string
}

type KeyValueList []KeyValue

func (kvl KeyValueList) Len() int           { return len(kvl) }
func (kvl KeyValueList) Less(i, j int) bool { return kvl[i].Key < kvl[j].Key }
func (kvl KeyValueList) Swap(i, j int)      { kvl[i], kvl[j] = kvl[j], kvl[i] }

func (kv *KeyValue) ToString() string {
	return fmt.Sprintf("%v %v", kv.Key, kv.Value)
}

func StringToKV(str string) (KeyValue, error) {
	// TODO: return proper error when fail
	kvpArr := strings.Fields(str)
	kv := KeyValue{Key: kvpArr[0], Value: kvpArr[1]}
	return kv, nil
}

func FileToKVL(file string) (KeyValueList, error) {
	cont := quickRead(file)
	lines := strings.Split(cont, "\n")
	kvl := []KeyValue{}
	for _, line := range lines {
		if line == "" {
			continue
		}
		kvp, ok := StringToKV(line)
		kvl = append(kvl, kvp)
		if ok != nil {
			return kvl, ok
		}
	}
	return kvl, nil
}

func quickRead(filename string) string {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatalf("cannot open %v", filename)
	}
	content, err := ioutil.ReadAll(file)
	if err != nil {
		log.Fatalf("cannot read %v", filename)
	}
	file.Close()
	return string(content)
}

func ShellCmd(cmd string) string {
	res := shellExec.Command("bash", "-c", cmd)
	stdout, _ := res.Output()
	return string(stdout)
}
